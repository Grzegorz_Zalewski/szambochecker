﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Microsoft.Win32;

namespace SzamboRuner
{
    internal static class Program
    {
        private const string UrlProtocol = "OA";

        private static void Main(string[] args)
        {
            if (!CheckForProtocolMessage())
            {
                Console.WriteLine(
                    "Hej wrzuć mnie do katalogu z quakiem uruchom w z uprawnieniami administratora i nacisnij jakiś przycisk");
                Console.ReadKey();
                RegisterUrlProtocol();
                Console.WriteLine(
                    "od teraz bedę uruchamiać twojego Quake z linków naciśnij jakiś przycisk aby zakończyć");
                Console.ReadKey();
            }
        }

        private static bool CheckForProtocolMessage()
        {
            var arguments = Environment.GetCommandLineArgs();
            if (arguments.Length <= 1) return false;
            // Format = "OA:Connect?id=127.0.0.1:27960"
            var args = arguments[1].Split(':');
            if (args[0].Trim().ToUpper() != "OA" || args.Length <= 1) return false;
            // Means this is a URL protocol
            var actionDetail = args[1].Split('?');
            if (actionDetail.Length <= 1) return false;
            switch (actionDetail[0].Trim().ToUpper())
            {
                case "CONNECT":
                    var details = actionDetail[1].Split('=');
                    if (details.Length > 1)
                    {
                        var ip = details[1].Trim();
                        var p = new Process
                        {
                            StartInfo =
                            {
                                FileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
                                           "\\openarena.exe",
                                Arguments = " +connect " + ip
                            }
                        };
                        p.Start();
                        // perform desire action
                        return true;
                    }
                    break;
            }
            return false;
        }

        private static void RegisterUrlProtocol()
        {
            var rKey = Registry.ClassesRoot.OpenSubKey(UrlProtocol, true);
            if (rKey == null)
            {
                rKey = Registry.ClassesRoot.CreateSubKey(UrlProtocol);
                rKey.SetValue("", "URL: OA Protocol");
                rKey.SetValue("URL Protocol", "");

                rKey = rKey.CreateSubKey(@"shell\open\command");
                rKey.SetValue("", "\"" + Assembly.GetExecutingAssembly().Location + "\" %1");
            }
            rKey.Close();
        }
    }
}