﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.AspNet.SignalR;
using szamboChecker.Models;
using szamboChecker.Service;

namespace szamboChecker
{
    public class SzamboHub : Hub
    {
        private List<string> Users = new List<string>();
        private static SzanboModel _lastSzambo = new SzanboModel {GameList = new List<Game>()};
        private static Timer _timer;

        private void Initialize()
        {
            if (Users.Count <= 0) return;
            {
                if (_timer == null)
                {
                    var BroadcastService = new BroadcastService();
                    _timer = new Timer {Interval = 15000, AutoReset = true};
                    _timer.Elapsed += BroadcastService.ListenAsync;
                    _timer.Start();
                }
                /* _timer = new Timer(BroadcastService.ListenAsync, null, 15000, Timeout.Infinite);
                _timer.AutoReset = true;
  */
            }
        }

        /// <summary>
        ///     The OnConnected event.
        /// </summary>
        /// <returns>
        ///     The <see cref="Task" />.
        /// </returns>
        public override Task OnConnected()
        {
            Clients.Caller.sendStatus(_lastSzambo);
            string clientId = GetClientId();

            if (Users.IndexOf(clientId) == -1)
            {
                Users.Add(clientId);
            }
            Initialize();
            return base.OnConnected();
        }

        public static void SzamboChanged(SzanboModel szambo)
        {
            //trzeci komparator
            //if (!SzamboComprator.Compare(_lastSzambo, szambo))
            //{
            _lastSzambo = szambo;
            IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<SzamboHub>();
            hubContext.Clients.All.sendStatus(_lastSzambo);
            //}

        }

        public static void StartIndicator()
        {
            IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<SzamboHub>();
            hubContext.Clients.All.StartIndicator();
        }

        /// <summary>
        ///     The OnReconnected event.
        /// </summary>
        /// <returns>
        ///     The <see cref="Task" />.
        /// </returns>
        public override Task OnReconnected()
        {
            string clientId = GetClientId();
            if (Users.IndexOf(clientId) == -1)
            {
                Users.Add(clientId);
            }
            Initialize();
            return base.OnReconnected();
        }

        /// <summary>
        ///     The OnDisconnected event.
        /// </summary>
        /// <returns>
        ///     The <see cref="Task" />.
        /// </returns>
        public override Task OnDisconnected()
        {
            string clientId = GetClientId();

            if (Users.IndexOf(clientId) > -1)
            {
                Users.Remove(clientId);
            }
            if (Users.Count > 0) return base.OnDisconnected();
            _timer.Stop();
            _timer.Dispose();
            _timer = null;

            return base.OnDisconnected();
        }

        /// <summary>
        ///     Get's the currently connected Id of the client.
        ///     This is unique for each client and is used to identify
        ///     a connection.
        /// </summary>
        /// <returns>The client Id.</returns>
        private string GetClientId()
        {
            string clientId = "";
            if (Context.QueryString["clientId"] != null)
            {
                // clientId passed from application 
                clientId = Context.QueryString["clientId"];
            }

            if (string.IsNullOrEmpty(clientId.Trim()))
            {
                clientId = Context.ConnectionId;
            }

            return clientId;
        }
    }
}