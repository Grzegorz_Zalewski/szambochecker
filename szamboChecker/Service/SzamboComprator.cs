﻿using System.Collections.Generic;
using System.Linq;
using szamboChecker.Models;

namespace szamboChecker.Service
{
    public static class SzamboComprator
    {
        public static bool Compare(SzanboModel m1, SzanboModel m2)
        {
            if (m1.Status == m2.Status)
                return false;
            if (m1.GameList.Count == m2.GameList.Count)
                return false;
            return !m2.GameList.Except(m1.GameList).Any();
        }
    }

    public class MultiSetComparer<T> : IEqualityComparer<IEnumerable<T>>
    {
        public bool Equals(IEnumerable<T> first, IEnumerable<T> second)
        {
            if (first == null)
                return second == null;

            if (second == null)
                return false;

            if (ReferenceEquals(first, second))
                return true;

            var firstCollection = first as ICollection<T>;
            var secondCollection = second as ICollection<T>;
            if (firstCollection != null && secondCollection != null)
            {
                if (firstCollection.Count != secondCollection.Count)
                    return false;

                if (firstCollection.Count == 0)
                    return true;
            }

            return !HaveMismatchedElement(first, second);
        }

        public int GetHashCode(IEnumerable<T> enumerable)
        {
            return enumerable.OrderBy(x => x).Aggregate(17, (current, val) => current*23 + val.GetHashCode());
        }

        private static bool HaveMismatchedElement(IEnumerable<T> first, IEnumerable<T> second)
        {
            int firstCount;
            int secondCount;

            Dictionary<T, int> firstElementCounts = GetElementCounts(first, out firstCount);
            Dictionary<T, int> secondElementCounts = GetElementCounts(second, out secondCount);

            if (firstCount != secondCount)
                return true;

            foreach (var kvp in firstElementCounts)
            {
                firstCount = kvp.Value;
                secondElementCounts.TryGetValue(kvp.Key, out secondCount);

                if (firstCount != secondCount)
                    return true;
            }

            return false;
        }

        private static Dictionary<T, int> GetElementCounts(IEnumerable<T> enumerable, out int nullCount)
        {
            var dictionary = new Dictionary<T, int>();
            nullCount = 0;

            foreach (T element in enumerable)
            {
                if (element == null)
                {
                    nullCount++;
                }
                else
                {
                    int num;
                    dictionary.TryGetValue(element, out num);
                    num++;
                    dictionary[element] = num;
                }
            }

            return dictionary;
        }
    }
}