﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Timers;
using szamboChecker.Enums;
using szamboChecker.Models;

namespace szamboChecker.Service
{
    public  class BroadcastService
    {
        private  UdpClient _listener;
        private  readonly IPEndPoint Address = new IPEndPoint(IPAddress.Any, 27960);
        private  List<string> _lastResponses = new List<string> {""};
        private  SzanboModel _szambo = new SzanboModel {GameList = new List<Game>()};
        private  List<string> _responseStrings = new List<string>();
        private  IPEndPoint _adressreciver = new IPEndPoint(IPAddress.Any, 27960);
        private  List<string> _adressList = new List<string>();


        public void ListenAsync(object state, ElapsedEventArgs elapsedEventArgs)
        {
            _responseStrings = new List<string>();
            _adressList = new List<string>();
            _adressreciver = new IPEndPoint(IPAddress.Any, 27960);
            _szambo = new SzanboModel {GameList = new List<Game>()};
                 
            if (_listener == null)
                Initialize();
            try
            {
                SzamboHub.StartIndicator();

                 SendRequest();

                while (true)
                {
                   
                        string result = Encoding.ASCII.GetString(_listener.Receive(ref _adressreciver));
                        if (result.Contains("infoResponse"))
                            if (!_responseStrings.Any(c => c == result))
                            {
                                _responseStrings.Add(result);
                                _adressList.Add(_adressreciver.ToString());
                            }
 
                }
               
            }
            catch (Exception)
            { 
                ReportProgress();
               
            }           
        }


        private  void ReportProgress()
        {
            // drugi komparator
            if (_lastResponses.Except(_responseStrings).Any() || _responseStrings.Except(_lastResponses).Any())
            {
                _lastResponses=_responseStrings;
                foreach (var responseString in _lastResponses)
                {
                    var details = StupidDictionaryParser(responseString);
                    var game = new Game
                    {
                        Ip = _adressList[_responseStrings.IndexOf(responseString)],
                        ConnectedPlayers = details[Keys.CurentClientsCount],
                        MaxPlayers = details[Keys.MaxClients],
                        Gametype = details[Keys.GameType],
                        MapName = details[Keys.MapName],
                        Hostmane = details[Keys.Hostname]
                    };
                    _szambo.GameList.Add(game);
                }
              
                SzamboHub.SzamboChanged(_szambo);
            }
        }

        private  void Initialize()
        {
            _listener = new UdpClient {Client = {ReceiveTimeout = 2000, ExclusiveAddressUse = false}};
            _listener.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            _listener.Client.Bind(Address);
        }

        private  Dictionary<string, string> StupidDictionaryParser(string inputtext)
        {
            inputtext = inputtext.Replace("????infoResponse\n", "");
            return inputtext.Split(new[] {"\\"}, StringSplitOptions.RemoveEmptyEntries).Select((s, i) => new {s, i})
                .GroupBy(x => x.i/2)
                .ToDictionary(g => g.First().s, g => g.Last().s);
        }

        private  void SendRequest()
        {
            var broadcastAddress = new IPEndPoint(IPAddress.Broadcast, 27960);
            var address = new IPEndPoint(IPAddress.Any, 27960);
            var client = new UdpClient {ExclusiveAddressUse = false};

            client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            client.Client.Bind(address);

            const string command = "xxxxgetinfo";
            byte[] dgram = Encoding.ASCII.GetBytes(command);
            dgram[0] = 0xFF;
            dgram[1] = 0xFF;
            dgram[2] = 0xFF;
            dgram[3] = 0xFF;

            client.Send(dgram, dgram.Length, broadcastAddress);
            client.Close();
        }
    }
}