﻿using System;
using System.Drawing;

namespace szamboChecker.Service
{
    public static class ColorHelper
    {
    internal static string SetColor(int p)
    {
        var red = p < 50 ? 255 : Math.Round(256 - (p - 50) * 5.12);
        var green = p > 50 ? 255 : Math.Round((p) * 5.12);
        return "rgb(" + red + "," + green + ",0)";
    }
    }
}