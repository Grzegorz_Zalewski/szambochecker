﻿using System;
using System.Collections.Generic;
using System.Linq;
using szamboChecker.Service;

namespace szamboChecker.Models
{
    public class SzanboModel
    {
        public List<Game> GameList { get; set; }

        public string Status
        {
            get { return GameList != null && GameList.Any() ? "Szambo wybiło" : "Nie ma szamba"; }
        }
    }


    public class Game
    {
        private string _type;
        public string Ip {  get; set; }
        public string MapName {  get; set; }

        public string MapImageUrl
        {
            get { return string.Concat("/Images/", MapName, ".jpg"); }
        }

        public string MaxPlayers {  get; set; }
        public string ConnectedPlayers {  get; set; }

        public string CounterColor
        {
            get
            {
                return
                    ColorHelper.SetColor(Convert.ToInt32(Convert.ToDecimal(ConnectedPlayers)/Convert.ToDecimal(MaxPlayers)*100));
            }
        }

        public string Gametype
        {
            get { return _type; }
            set { _type = GameTypes.GetGameType(value); }
        }

        public string Hostmane { get; set; }

        public string LaunchUrl
        {
            get { return @"oa:Connect?id=" + Ip; }
        }
    }


    public static class GameTypes
    {
        private static readonly string[] Gametypes =
        {
            "DM", "Tour.", "SP DM", "Team DM", "CTF", "OFC", "Overload",
            "Harvester", "Elimin.", "CTF Elimin.", "LMS", "DD", "D"
        };

        public static string GetGameType(string gameTypeId)
        {
            return Gametypes[Convert.ToInt16(gameTypeId)];
        }
    }
}