﻿using System;

namespace szamboChecker.Enums
{
    public static class Keys
    {
        public static String Voip
        {
            get { return "voip"; }
        }

        public static String HumanPlayers
        {
            get { return "g_humanplayers"; }
        }

        public static String PasswordProtect
        {
            get { return "g_needpass"; }
        }

        public static String Pure
        {
            get { return "pure"; }
        }

        public static String GameType
        {
            get { return "gametype"; }
        }

        public static String MaxClients
        {
            get { return "sv_maxclients"; }
        }

        public static String CurentClientsCount
        {
            get { return "clients"; }
        }

        public static String MapName
        {
            get { return "mapname"; }
        }

        public static String Hostname
        {
            get { return "hostname"; }
        }

        public static String Protocol
        {
            get { return "protocol"; }
        }
    }
}